from django.db import models
""" from django.urls import reverse """

# Create your models here.


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    shoe_picture = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
    )


    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_shoes")
